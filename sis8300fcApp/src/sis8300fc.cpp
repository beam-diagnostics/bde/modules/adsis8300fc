/* sis8300fc.cpp
 *
 * This is a driver for a FC based on Struck SIS8300 digitizer.
 *
 * Author: Hinko Kocevar
 *         ESS ERIC, Lund, Sweden
 *
 * Created:  November 22, 2017
 *
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <string>
#include <stdarg.h>

#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <iocsh.h>

#include <asynNDArrayDriver.h>
#include <epicsExport.h>

#include <sis8300fc.h>


static const char *driverName = "sis8300fc";

/** Constructor for sis8300fc; most parameters are simply passed to sis8300::sis8300.
  * After calling the base class constructor this method creates a thread to compute the simulated detector data,
  * and sets reasonable default values for parameters defined in this class and SIS8300.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] devicePath The path to the /dev entry.
  * \param[in] numSamples The initial number of samples.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
sis8300fc::sis8300fc(const char *portName, const char *devicePath,
        int numSamples, int maxBuffers, size_t maxMemory,
        int priority, int stackSize)

    : sis8300(portName, devicePath,
            numSamples,
            0,
            maxBuffers, maxMemory,
            priority, stackSize)

{
    asynPrintDeviceInfo(pasynUserSelf, "nothing to do here..");
}

sis8300fc::~sis8300fc()
{
    asynPrintDeviceInfo(pasynUserSelf, "nothing to do here..");
}

// report status of the driver, prints details about the driver if details>0.
void sis8300fc::report(FILE *fp, int details)
{
    // invoke the base class method
    sis8300::report(fp, details);
}

// configuration command, called directly or from iocsh
extern "C" int sis8300fcConfig(const char *portName, const char *devicePath,
        int numSamples, int maxBuffers, int maxMemory,
        int priority, int stackSize)
{
    new sis8300fc(portName, devicePath,
            numSamples,
            (maxBuffers < 0) ? 0 : maxBuffers,
            (maxMemory < 0) ? 0 : maxMemory,
            priority, stackSize);
    return(asynSuccess);
}

/** Code for iocsh registration */
static const iocshArg arg0 = {"Port name",     iocshArgString};
static const iocshArg arg1 = {"Device path",   iocshArgString};
static const iocshArg arg2 = {"Num samples",   iocshArgInt};
static const iocshArg arg3 = {"maxBuffers",    iocshArgInt};
static const iocshArg arg4 = {"maxMemory",     iocshArgInt};
static const iocshArg arg5 = {"priority",      iocshArgInt};
static const iocshArg arg6 = {"stackSize",     iocshArgInt};
static const iocshArg * const configArgs[] =  {
    &arg0, &arg1, &arg2, &arg3, &arg4, &arg5, &arg6};
static const iocshFuncDef config = {"sis8300fcConfigure", 7, configArgs};
static void configCallFunc(const iocshArgBuf *args)
{
    sis8300fcConfig(args[0].sval, args[1].sval, args[2].ival, args[3].ival,
            args[4].ival, args[5].ival, args[6].ival);
}

static void sis8300fcRegister(void)
{
    iocshRegister(&config, configCallFunc);
}

extern "C" {
epicsExportRegistrar(sis8300fcRegister);
}
