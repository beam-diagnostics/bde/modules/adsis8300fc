/* sis8300fc.h
 *
 * This is a driver for a FC based on Struck SIS8300 digitizer.
 *
 * Author: Hinko Kocevar
 *         ESS ERIC, Lund, Sweden
 *
 * Created:  November 22, 2017
 *
 */

#include <stdint.h>
#include <epicsEvent.h>
#include <epicsTime.h>
#include <asynNDArrayDriver.h>

#include <sis8300.h>

class epicsShareClass sis8300fc : public sis8300 {
public:
    sis8300fc(const char *portName, const char *devicePath,
            int numSamples, int maxBuffers, size_t maxMemory,
            int priority, int stackSize);
    virtual ~sis8300fc();

    // these are the methods that we override from sis8300
    virtual void report(FILE *fp, int details);

protected:

private:

};

